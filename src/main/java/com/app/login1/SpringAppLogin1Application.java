package com.app.login1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAppLogin1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringAppLogin1Application.class, args);
	}

}
