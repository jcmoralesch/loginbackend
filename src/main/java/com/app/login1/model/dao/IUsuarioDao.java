package com.app.login1.model.dao;

import org.springframework.data.repository.CrudRepository;
import com.app.login1.model.entity.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
	
	public Usuario findByUsername(String username);

}
