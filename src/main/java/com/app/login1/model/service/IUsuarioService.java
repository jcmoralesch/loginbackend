package com.app.login1.model.service;

import com.app.login1.model.entity.Usuario;

public interface IUsuarioService {
	public Usuario findByUsername(String username);
}
